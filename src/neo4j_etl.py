# Import Packages
## Standard Packages
import re
import hashlib

## Installed Packages
from neo4j import GraphDatabase


# Neo4j version: 4.1.3


def create_hash_id(*args):
    """Converts one or more arguments into a hash ID.
    :param args: One or more arguments (ints, strings, etc.)
    :rtype: String representing hash of concatenated input
    """
    # Ensure that input is a string
    input_string = ''
    for arg in args:
        arg = str(arg)
        arg = arg.strip()
        input_string += arg

    input_string = str(input_string)

    # Encode string into a sequence of bytes
    input_string = input_string.encode()

    # Run MD5 hashing algorithm
    hash_object = hashlib.md5(input_string)

    return hash_object.hexdigest()


assert create_hash_id('Hello World') == 'b10a8db164e0754105b7a99be72e3fe5'


def format_neo4j_props(dictionary):
    """Format node properties for Neo4j
    :param dictionary: a dictionary of node attributes
    :rtype: String representing a Neo4j Cypher query for node properties
    """
    return '{%s}' % ', '.join(["""%s: \"%s\"""" % (k, v) for k, v in dictionary.items()])


assert format_neo4j_props({"name": "Joe", "node_type": "Author"}) == '{name: "Joe", node_type: "Author"}'


def format_neo4j_name(name):
    """Format node name for Neo4j.
    :param name: String representing name of a node
    :rtype: String representing processed version of node name
    """
    name = name.replace('(', '')
    name = name.replace(')', '')
    name = name.replace(' ', '_')
    name = name.replace('.', '')
    name = name.replace("-", "")
    name = name.replace(" ", "_")
    name = name.replace('"', "'")
    name = re.sub("[^_A-Za-z0-9]", "", name)
    name_match = re.match('[0-9]', name)
    if name_match:
        name = '_' + name
    return name


assert format_neo4j_name('Ontology (information science)') == 'Ontology_information_science'
assert format_neo4j_name('1969 establishments in the United States') == '_1969_establishments_in_the_United_States'


### Inspired by neonx package
def get_node(node_id, properties):
    """reformats a NetworkX node for `update_database()`.
    :param node_id: the index of a NetworkX node
    :param properties: a dictionary of node attributes
    :rtype: String representing a Neo4j Cypher query to create a node
    """
    if 'node_type' not in properties.keys():
        return "\n"
    return "CREATE (`{0}`:{1} {2})\n".format(create_hash_id(node_id), properties['node_type'],
                                             format_neo4j_props(properties))


assert get_node(0, {"name": "Joe",
                    "node_type": "Author"}) == 'CREATE (`cfcd208495d565ef66e7dff9f98764da`:Author {name: "Joe", node_type: "Author"})\n'


### Inspired by neonx package
# TODO: Incorporate `from_article` property for relations
def get_relationship(from_id, to_id, properties):
    """reformats a NetworkX edge for `update_database()`.
    :param from_id: the ID of a NetworkX source node
    :param to_id: the ID of a NetworkX target node
    :param rel_name: string that describes the relationship between the
        two nodes
    :param properties: a dictionary of edge attributes
    :rtype: String representing a Neo4j Cypher query to create an edge
    """
    if 'relation' not in properties.keys():
        return ""

    return "CREATE (`{0}`)-[:RELATION {{relation:'{2}'}}]->(`{1}`)\n".format(create_hash_id(from_id),
                                                                             create_hash_id(to_id),
                                                                             properties['relation'])


assert get_relationship(0, 1, {
    "relation": "LINKS_TO"}) == "CREATE (`cfcd208495d565ef66e7dff9f98764da`)-[:RELATION {relation:'LINKS_TO'}]->(`c4ca4238a0b923820dcc509a6f75849b`)\n"


def update_database(G, uri, user, pwd):
    """Ingests a NetworkX graph into Neo4j graph database instance.
    :param G: NetworkX graph
    :param uri: Neo4j URI
    :param user: Neo4j database username
    :param pwd: Neo4j database password
    :rtype: None
    """
    driver = GraphDatabase.driver(uri, auth=(user, pwd), encrypted=False)
    session = driver.session()

    query = ""
    nodes = {}
    for i, (node_name, properties) in enumerate(G.nodes(data=True)):
        query += get_node(i, properties)
        nodes[node_name] = i

    for from_node, to_node, properties in G.edges(data=True):
        query += get_relationship(nodes[from_node], nodes[to_node],
                                  properties)

    session.run(query)
    session.close()
